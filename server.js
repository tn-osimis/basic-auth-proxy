'use strict'

const proxy = require('http-proxy')
const uuid = require('uuid/v4')

// https://tools.ietf.org/html/rfc5234#appendix-B.1 CTL ("controls")
function controlChar(char) {
	return char >= '\x00' && char <= '\x1F' || char == '\x7F'
}

const target = process.env.TARGET
const userId = process.env.USER_ID
const password = process.env.PASSWORD
const port = process.env.PORT || 80

if (!target) {
	console.error('TARGET is undefined')
	process.exit(1)
}

console.log(`TARGET: ${target}`)

if (!userId) {
	console.error('USER_ID undefined')
	process.exit(2)
}

// https://tools.ietf.org/html/rfc7617#section-2
if (Array.from(userId).some(controlChar)) {
	console.error('USER_ID contains control characters')
	process.exit(3)
}

// https://tools.ietf.org/html/rfc7617#section-2
if (userId.includes(':')) {
	console.error('USER_ID contains a colon')
	process.exit(4)
}

console.log(`USER_ID: ${userId}`)

if (!password) {
	console.error('PASSWORD is undefined')
	process.exit(5)
}

const server = proxy.createProxyServer({
	target: target,
	xfwd: true,
	changeOrigin: true, // FIXME remove
	auth: userId + ':' + password
})

server.on('error', (err, req, res) => {
	const eventId = uuid()
	console.log(`${eventId}: ${err.toString() || 'Undescribed error'}`)
	res.writeHead(500, {'Content-Type': 'text/plain'})
	res.end(`Proxying error.\nEvent ID: ${eventId}.`)
})

server.on('close', () => {
	console.log('Close event observed')
})

process.on('SIGINT', () => {
	console.log('Closing server ...')
	server.close()
})

server.listen(port)
console.log(`Listening on TCP port ${port}`)
