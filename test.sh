#!/usr/bin/env bash

# Requirements:
# - bash
# - docker
# - docker-compose
# - cut, base64 (tested with coreutils implementations)

set -o errexit

function run {
	docker container run --rm --interactive --network=bap_default "$@"
}

function service-wait {
	run docker.io/willwill/wait-for-it:latest "$@"
}

function http {
	run docker.io/alpine/httpie:latest --ignore-stdin "$@"
}

function jq {
	run docker.io/stedolan/jq:latest --raw-output "$@"
}

real_cut=$(type -p cut)
function cut {
	"$real_cut" --delimiter=" " "$@"
}

export COMPOSE_PROJECT_NAME=bap
export COMPOSE_FILE=test.yml
export USER_ID=foo
export PASSWORD=bar
docker-compose up --build -d
trap 'docker-compose down -v' EXIT
service-wait target:80
service-wait sut:80

# The test proxy target answers with a JSON object echoing the headers of the
# request, allowing us to test if the proper Authorization header value reaches
# the targets.

IFS=: read -r userid password <<<"$(http sut/get | \
                                    jq .headers.Authorization | \
                                    cut --field=2 | \
                                    base64 --decode)"

if [[ $userid != "$USER_ID" || $password != "$PASSWORD" ]]; then
	expected=$USER_ID:$PASSWORD
	actual=$userid:$password
	echo "Test failed: expected \"$expected\", got \"$actual\""
	exit 1
fi

echo OK
