#!/usr/bin/env bash
set -o errexit
image=docker.io/osimis/basic-auth-proxy:$(get-version.sh)
docker image build --tag="$image" .
docker image push "$image"
