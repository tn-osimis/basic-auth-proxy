#!/usr/bin/bash
set -o errexit
docker container run --rm --interactive docker.io/stedolan/jq:latest --raw-output .version <package.json
